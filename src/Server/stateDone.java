package Server;

public class stateDone implements iState {

	private static stateDone stDone=null;
	
	public static synchronized stateDone getStateDone(){
		if (stDone==null) stDone=new stateDone();
		return stDone;
	}
	private stateDone(){
		
	}
	
	@Override
	public String getState() {
		return "Done";
	}
}
