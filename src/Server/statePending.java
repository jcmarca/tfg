package Server;

public class statePending implements iState {

	private static statePending stPending=null;
	
	public static synchronized statePending getStatePending(){
		if (stPending==null) stPending=new statePending();
		return stPending;
	}
	private statePending(){
		
	}
	
	@Override
	public String getState() {
		return "Pending";
	}

	
}
