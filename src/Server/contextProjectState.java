package Server;

public class contextProjectState {

	private iState state;
	
	public contextProjectState(){
		//on create, the default state is pending
		this.state= statePending.getStatePending();
	}

	public void setProcessed() throws serverException{
		this.state= stateProcessed.getStateProcessed();
	}

	public String getState(){
		return state.getState();
	}
}
