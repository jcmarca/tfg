package Server;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class serverController {
	
	private HashMap<UUID, project> projects; //contains the projects
	private HashMap<UUID, UUID> taskProject; //contains the relationship between a task and a project
	private EntityManagerFactory emf;
	private EntityManager em;
	private EntityTransaction tx =null;
	private String tmpPath="";
	private String dexPath="";
	
	
	private static serverController svrCtl=null;
	
	public static synchronized serverController getServerController(){
		if (svrCtl==null) svrCtl=new serverController();
		return svrCtl;
	}
	
	private serverController(){
		projects= new LinkedHashMap<UUID, project>();
		taskProject= new HashMap<UUID, UUID>();
		emf = Persistence.createEntityManagerFactory("MGCServer"); 
		em=emf.createEntityManager();
	}
	public void loadConfig(){
		Properties parameters = new Properties();
		try {
			parameters.load(new FileInputStream("/opt/MGCServer/config.data"));
			tmpPath = parameters.getProperty("tmpPath");
			System.out.println (tmpPath);
			dexPath = parameters.getProperty("dexPath");
			System.out.println (dexPath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	public String getTmpPath() {
		return tmpPath;
	}

	public String getDexPath() {
		return dexPath;
	}

	public void loadDataFromDB() throws serverException {
		
		TypedQuery<task> query=em.createQuery("SELECT tsk FROM task tsk WHERE tsk.taskState <> :tskState",task.class);
		query.setParameter("tskState","Done");
		List<task> taskList=query.getResultList();
		int cont=0;
		for (task tsk : taskList){
			cont++;
			switch(tsk.getTaskState()){
				case "Pending":
					tsk.setPending();
					break;
				case "inProcess":
					tsk.setPending();
					tsk.setInProcessState();
					break;
				case "Processed":
					tsk.setPending();
					tsk.setInProcessState();
					tsk.setProcessedState();
					break;
				case "Done":
					tsk.setPending();
					tsk.setInProcessState();
					tsk.setProcessedState();					
					tsk.setDoneState();
					break;
			}
			//project exists?
			project prj=null;
			UUID idProject= tsk.getIdProject();
			if (projects.isEmpty() || !projects.containsKey(idProject)){
				prj = em.find(project.class, idProject);
				prj.setEntityManager(em);
				projects.put(idProject, prj);
				
			}
			else{
				prj=projects.get(idProject);
			}
			
			if (prj!=null){
				//File class exists?
				UUID idFileClass = tsk.getidFileClass();
				fileClass fClass=null;
				if (!prj.containsFileClass(idFileClass)){
					fClass=em.find(fileClass.class, idFileClass);
					prj.addFileClass(fClass);
				}
				
				//if (fClass!=null && !prj.containsTask(tsk.getIdTask())){
				if (!prj.containsTask(tsk.getIdTask())){
					prj.addTask(tsk);
					System.out.println("Loading task: " + tsk.getIdTask());
					taskProject.put(tsk.getIdTask(), prj.getIdProject());
				}
			}
		}
		System.out.println("Contador: " + cont);
	}
	
	public String getFileClassName(UUID idTask){
		UUID idProject=null;
		project prj = null;
		String fClassName="";
				
		idProject = taskProject.get(idTask);
		if (idProject!=null) prj = projects.get(idProject);
		if (prj!=null) fClassName=prj.getFileClassName(idTask);
		return fClassName;
		
	}
	
	public String getJarName(UUID idTask){
		UUID idProject=null;
		project prj = null;
		String JarName="";
				
		idProject = taskProject.get(idTask);
		if (idProject!=null) prj = projects.get(idProject);
		if (prj!=null) JarName=prj.getJarName(idTask);
		return JarName;
		
	}
	
	public UUID newTask(UUID idProject, byte[] objToProcess, UUID idFileClass) throws serverException{
		project prj=null;
		UUID idTask=null;

		if (idProject==null) throw new serverException ("serverController.newTask.idProject can not be null");
		if (objToProcess==null) throw new serverException ("serverController.newTask.objToProcess can not be null");
		if (idFileClass==null) throw new serverException ("serverController.newTask.idFileClass can not be null");
		
		prj=projects.get(idProject);
		if (prj==null)	throw new serverException("serverController.newTask project not found");
		
		
		idTask = prj.addTask(objToProcess, idFileClass);
		if (idTask==null)	throw new serverException("serverController.newTask task not added");
		
		taskProject.put(idTask, idProject);
		
		return idTask;
	}
	
	public UUID newFileClass(UUID idProject, byte[] fClass, String className) throws serverException{
		project prj;
		UUID idFileClass=null;
		if (idProject==null) throw new serverException ("serverController.newFileClass.idProject can not be null");
		if (fClass==null) throw new serverException ("serverController.newFileClass.fClass can not be null");
		
		prj=projects.get(idProject);
		if (prj==null) {
			prj = em.find(project.class, idProject);
			if (prj==null) {
				throw new serverException("serverController.newFileClass project not found");
			}else{
				prj.setEntityManager(em);
				projects.put(idProject, prj);
			}
		}
		idFileClass=prj.addFileClass(fClass, className, tmpPath, dexPath);
		return idFileClass;
	}
	
	
	public UUID getIdObjToProcess() throws Exception{
		UUID idTask =null;
		project prj=null;
		task tsk = null;

		//find the first Pending project 
		Iterator<project> itProject= projects.values().iterator();
		while (itProject.hasNext() && idTask==null) {
			prj = itProject.next();
			tsk = prj.getTaskToProcess();
			if (tsk !=null) idTask = tsk.getIdTask();
		}
		
		return idTask;

	}
	
	public byte[] getObjToProcess(UUID idTask) throws serverException{
		UUID idProject =null;
		project prj = null;
		task tsk=null;
		byte[] objToProcess=null;
		
		idProject = taskProject.get(idTask);
		if (idProject!=null) prj = projects.get(idProject);
		if (prj!=null)	tsk = prj.getTask(idTask);
		if (tsk!=null)	objToProcess = tsk.getObjToProcess();
				
		return objToProcess;
	}
	
	public byte[] getClassToProcess(UUID idTask){
		UUID idProject=null;
		project prj = null;
		fileClass fClass=null;
		byte[] classToProcess=null;
		
		idProject = taskProject.get(idTask);
		if (idProject!=null) prj = projects.get(idProject);
		if (prj!=null) fClass=prj.getFileClass(idTask);
		if (fClass!=null) classToProcess = fClass.getFileClass();
		
		return classToProcess;
	}
	
	public byte[] getProcessedObj(UUID idTask){
		byte[] processedObj=null;
		project prj = null;
		task tsk=null;
		UUID idPrj=taskProject.get(idTask);
		prj=projects.get(idPrj);
		if (prj!=null) tsk=prj.getProcessedTask(idTask);
		if (tsk!=null)	processedObj=tsk.getProcessedObj();
		
		return processedObj;
	}
	
	public void setProcessedObj(UUID idTask, byte[] processedObj) throws serverException{
		UUID idProject=null;
		project prj=null;
		
		if (idTask==null) throw new serverException("serverController.setProcessedObj.idTask can not be null");
		if (processedObj==null) throw new serverException("serverController.setProcessedObj.processedObj can not be null");
		
		idProject = taskProject.get(idTask);
		if (idProject==null) throw new serverException("serverController.setProcessedObj can not find the associated project");
		
		prj=projects.get(idProject);
		if (prj==null) throw new serverException("serverController.setProcessedObj project not found");
		
		prj.setProcessedTask(processedObj, idTask);
	}
	
	public void deleteTask(UUID idTask) throws serverException{
		UUID idProject=null;
		project prj=null;
		
		if (idTask==null) throw new serverException ("serverController.deleteTask.idTask can not be null");
		
		idProject = taskProject.get(idTask);
		if (idProject==null) throw new serverException ("serverController.deleteTask can not find idProject");
		
		prj=projects.get(idProject);
		if (prj==null) throw new serverException ("serverController.deleteTask project not found");
		
		prj.deleteTask(idTask);
	}
	
	public UUID createProject(){
		UUID idProject = UUID.randomUUID();
		project prj = new project(idProject, em);
		projects.put(idProject, prj);
		tx=em.getTransaction(); 
		tx.begin();
	    try { 
	        em.persist(prj); 
	        tx.commit(); 
	    } catch(Exception e) { 
	         tx.rollback(); 
	    } 
		return idProject;
	}
}
