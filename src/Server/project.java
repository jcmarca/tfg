package Server;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class project {
	@Id
	private UUID idProject;
	
	@Transient
	private HashMap<UUID,task> tasks; // contains the tasks of the project
	@Transient
	private HashMap<UUID, fileClass> fClasses; //Contains the classes of the project
	
	@Transient
	private contextProjectState cProjectState;
	@Transient
	private EntityManager em;
	@Transient
	private EntityTransaction tx =null;
	
	public project(){
		tasks = new LinkedHashMap<UUID,task>();
		fClasses = new HashMap<UUID, fileClass>();
		cProjectState=new contextProjectState();
	}
	
	public project(UUID idProject, EntityManager em){
		this.idProject = idProject;
		this.em = em;
		tasks = new LinkedHashMap<UUID,task>();
		fClasses = new HashMap<UUID, fileClass>();
		cProjectState=new contextProjectState();
	}
	
	public void setEntityManager(EntityManager em){
		this.em = em;
	}
	
	public UUID getIdProject() {
		return idProject;
	}

	public String getFileClassName(UUID idTask){
		String fcName="";
		task tsk = tasks.get(idTask);
		UUID idFileClass = tsk.getidFileClass();
		fileClass fc= fClasses.get(idFileClass);
		fcName = fc.getfClassName();
		return fcName;
	}
	
	public String getJarName(UUID idTask){
		String JarName="";
		task tsk = tasks.get(idTask);
		UUID idFileClass = tsk.getidFileClass();
		fileClass fc= fClasses.get(idFileClass);
		JarName = fc.getJarName();
		return JarName;
	}
	
	public void addFileClass(fileClass fClass){
		UUID idFileClass = fClass.getIdFileClass();
		fClasses.put(idFileClass, fClass);
	}
	
	public void addTask(task tsk){
		UUID idTask = tsk.getIdTask();
		tasks.put(idTask,tsk);
	}
	
	public UUID addFileClass(byte[] fClass, String className, String tmpPath, String dexPath) throws serverException{
		fileClass fC = new fileClass(fClass, idProject, className, tmpPath, dexPath);
		UUID idFileClass = fC.getIdFileClass();
		fClasses.put(idFileClass, fC);
		//Persistence
		tx=em.getTransaction(); 
		tx.begin();
	    try { 
	        em.persist(fC); 
	        tx.commit(); 
	    } catch(Exception e) { 
	         tx.rollback(); 
	    } 
	   
		return idFileClass;
	}
	
	
	
	
	public UUID addTask(byte[] objToProcess, UUID idFileClass) throws serverException{
		
		tx=em.getTransaction(); 
		tx.begin();
		//create a new task and add it to the collection
		
		task tsk= new task(objToProcess, idFileClass, idProject);
		
	    try { 
	        em.persist(tsk); 
	        tx.commit(); 
	    } catch(Exception e) { 
	         tx.rollback(); 
	    } 
		tasks.put(tsk.getIdTask(),tsk);

		return tsk.getIdTask();
	}
	
	public task getTaskToProcess() throws serverException{
		Iterator<task> iterator;
		task tskTmp, tsk=null;
		
		//get the first task pending to process
		iterator = tasks.values().iterator();
		while (iterator.hasNext() && tsk==null) {
			tskTmp = iterator.next();
			if (tskTmp.getState().equals("Pending")){
				tskTmp.setInProcessState();
				tsk=tskTmp;
				//Persistence
				updateTask(tsk);
			}
		}
		//if there are no pending tasks then get the first "In process" task.
		if (tsk==null) {
			iterator = tasks.values().iterator();
			while (iterator.hasNext()) {
				tskTmp = iterator.next();
				if (tskTmp.getState().equals("inProcess")){
					tsk=tskTmp;
				}
			}
		}
		return tsk;
	}
	
	public fileClass getFileClass(UUID idTask){
		task tsk = tasks.get(idTask);
		UUID idFileClass = tsk.getidFileClass();
		return fClasses.get(idFileClass);
	}
	
	public task getProcessedTask(UUID idTask){
		task tsk = tasks.get(idTask);
		if (tsk.getState().equals("Processed")){
			return tsk;
		}
		return null;
	}
	
	public void deleteTask(UUID idTask) throws serverException{
		task tsk=tasks.get(idTask);
		tsk.setDoneState();
		//Persistence
		updateTask(tsk);
		tasks.remove(idTask);

	}
	
	public void setProcessedTask(byte[] processedObj, UUID idTask) throws serverException{
		task tsk=tasks.get(idTask);
		//adds the processed file only if the task is in "inProcess" state
		if (tsk.getState().equals("inProcess")){
			tsk.setProcessedObj(processedObj);
			tsk.setProcessedState();
			//Persistence
			updateTask(tsk);
			
		}
	}
	
	public void setProcessedProject() throws serverException{
		cProjectState.setProcessed();
	}
	
	public String getProjectState(){
		return cProjectState.getState();
	}
	
	public task getTask(UUID idTask){
		task tsk = tasks.get(idTask);
		return tsk;
	}
	
	public boolean containsFileClass(UUID idFileClass){
		boolean ret=false;
		if (!fClasses.isEmpty()) ret=fClasses.containsKey(idFileClass);
		return ret;
	}
	
	public boolean containsTask(UUID idTask){
		boolean ret=false;
		if (!tasks.isEmpty()) ret = tasks.containsKey(idTask);
		return ret;
	}
	
	private void updateTask(task tsk){
		//Persists the modifications of the task
		tx=em.getTransaction(); 
		tx.begin();
		try{
			em.find(task.class, tsk.getIdTask());
			tx.commit(); 
		}
		catch(Exception e){
			tx.rollback();
		}
	}
}
