package Server;

public class serverException extends Exception {

	private static final long serialVersionUID = 1L;

	public serverException(String msg){
		super(msg);
	}
}
