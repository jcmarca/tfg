package Server;

public class stateProcessed implements iState {

	private static stateProcessed stProcessed=null;
	
	public static synchronized stateProcessed getStateProcessed(){
		if (stProcessed==null) stProcessed=new stateProcessed();
		return stProcessed;
	}
	private stateProcessed(){
		
	}
	
	@Override
	public String getState() {
		return "Processed";
	}

}
