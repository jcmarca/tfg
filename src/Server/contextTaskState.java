package Server;

public class contextTaskState {
	private iState state;
	
	public contextTaskState(){}
	
	public void setPending() throws serverException{
		this.state= statePending.getStatePending();
	}	
	
	public void setInProcess() throws serverException{
		if (state.getState().equals("Pending"))
			this.state= stateInProcess.getStateInProcess();
		else throw new serverException("Previous state is not 'Pending'");
	}
	
	public void setProcessed() throws serverException{
		if (state.getState().equals("inProcess"))
			this.state= stateProcessed.getStateProcessed();
		else throw new serverException("Previous state is not 'in Process'");
	}
	
	public void setDone() throws serverException{
		if (state.getState().equals("Processed"))
			this.state= stateDone.getStateDone();
		else throw new serverException("Previous state is not 'Processed'");
	}
	
	public String getState(){
		return state.getState();
	}
}
