package Server;


import java.util.UUID;

import javax.persistence.*;


@Entity
public class task {
	@Id
	private UUID idTask;
	


	private byte[] objToProcess;
	private byte[] processedObj;
	private UUID idFileClass;
	private UUID idProject;
	private String taskState;
	@Transient
	private contextTaskState cTaskState;
	
	
	public task(){
		this.cTaskState = new contextTaskState();
	
	}
	
	public task(byte[] objToProcess, UUID idFileClass, UUID idProject) throws serverException {
		if (objToProcess==null) throw new serverException("task.objToProcess can't be null");
		if (idFileClass==null) throw new serverException("task.idClass can't be null");
		if (idProject==null) throw new serverException("task.idProject can't be null");
		idTask = UUID.randomUUID();
		this.cTaskState = new contextTaskState();
		this.idFileClass=idFileClass;
		this.idProject = idProject;
		this.objToProcess = objToProcess;
		this.processedObj = null;
		cTaskState.setPending();
		taskState=cTaskState.getState();
	}
	
	public void setProcessedObj(byte[] pObj) throws serverException{
		if (pObj!=null) this.processedObj=pObj;
		else throw new serverException("task.setProcessedObj.pObj can't be null");
	}

	public byte[] getObjToProcess() {
		return objToProcess;
	}

	public byte[] getProcessedObj() {
		return processedObj;
	}
	
	public String getState(){
		return cTaskState.getState();
	}
	
	public void setPending() throws serverException{
		cTaskState.setPending();
		taskState=cTaskState.getState();
	}
	
	public void setInProcessState() throws serverException{
		cTaskState.setInProcess();
		taskState=cTaskState.getState();
	}
	
	public void setProcessedState() throws serverException{
		cTaskState.setProcessed();
		taskState=cTaskState.getState();
	}
	
	public void setDoneState() throws serverException{
		cTaskState.setDone();
		taskState=cTaskState.getState();
	}
	
	public UUID getIdTask(){
		return idTask;
	}
	
	public UUID getidFileClass(){
		return idFileClass;
	}
	
	public UUID getIdProject(){
		return idProject;
	}
	
	public void setTaskState(String taskState){
		this.taskState=taskState;
	}
	public String getTaskState(){
		return taskState;
	}
	
	public void setidFileClass(UUID idFileClass){
		this.idFileClass = idFileClass;
	}
	public void setIdTask(UUID idTask) {
		this.idTask = idTask;
	}

}
