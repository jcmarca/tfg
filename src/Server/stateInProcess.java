package Server;

public class stateInProcess implements iState {

	private static stateInProcess stInProcess=null;
	
	public static synchronized stateInProcess getStateInProcess(){
		if (stInProcess==null) stInProcess=new stateInProcess();
		return stInProcess;
	}
	private stateInProcess(){
		
	}
	
	@Override
	public String getState() {
		return "inProcess";
	}

}
