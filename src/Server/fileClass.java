package Server;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class fileClass {

	@Id
	private UUID idFileClass;
	private byte[] fClass;
	private UUID idProject;
	private String fClassName;
	private String jarName;
	
	public String getJarName() {
		return jarName;
	}

	public void setJarName(String jarName) {
		this.jarName = jarName;
	}

	public fileClass(){
		//Empty constructor
	}
	
	public fileClass(byte[] fClass, UUID idProject, String className, String tmpPath, String dexPath) throws serverException{
		if (fClass==null) throw new serverException("fileClass.fClass can't be null");
		if (idProject==null) throw new serverException("fileClass.idProject can't be null");
		//convert class to dex for android
		try {
			this.fClass=getDexJar(fClass, className, tmpPath, dexPath);
			this.fClassName=className;
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		idFileClass = UUID.randomUUID();

		this.idProject=idProject;
	}
	
	private byte[] getDexJar(byte[] fClass, String className, String tmpPath, String dexPath) 
									throws IOException, InterruptedException{
		
		//remove extension from className
		String classNameNoExt = className;
		boolean swExt =false;
		int findDot = classNameNoExt.lastIndexOf('.');
		if (findDot>0){
			swExt=true;
			classNameNoExt=classNameNoExt.substring(0, findDot);
		}
		
		//if file name has no extension add it
		if (swExt==false){
			className= className + ".class";
		}
		
		//save class to disc
		File fc = new File(tmpPath + className);
		FileOutputStream fos = new FileOutputStream(fc);
		fos.write(fClass);
	    fos.close();
		
	    //create jar with dx command line
		String command = dexPath + "dx --dex --no-strict --output=" 
				+ tmpPath +  classNameNoExt + ".jar " + tmpPath + className;
		 
		Process p;
		p = Runtime.getRuntime().exec(command);
		p.waitFor();

		//read jar to byte array
		File jarFile = new File(tmpPath +  classNameNoExt +".jar");
        FileInputStream fis = new FileInputStream(jarFile);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[(int)jarFile.length()];
        try {
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum); //no doubt here is 0
            }
        } catch (IOException e) {
        	e.printStackTrace();
        }
        byte[] bytes = bos.toByteArray();
        
        
        this.jarName= classNameNoExt +".jar";
        
  
		//return byte array
		return bytes;
	}
	
	public byte[] getFileClass(){
		return fClass;
	}
	
	public UUID getIdFileClass(){
		return idFileClass;
	}
	
	public String getfClassName() {
		return fClassName;
	}

	public void setfClassName(String fClassName) {
		this.fClassName = fClassName;
	}

	public UUID getIdProject(){
		return idProject;
	}

}
