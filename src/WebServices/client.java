package WebServices;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataParam;
import Server.serverException;




@Path("/client/")
public class client extends absService {

	@GET
	@Path("getIdObjToProcess")
	@Produces(MediaType.TEXT_PLAIN)
	public String getIdObjToProcess(){
		UUID idTsk=null;
		try {
			idTsk = super.sCtl.getIdObjToProcess();
			if (idTsk==null) return "";
		} catch (serverException e) {
			return "";
		} catch (Exception e) {
			return "";
		}
		return idTsk.toString();
	}
	
	@GET
	@Path("getFileClassName")
	@Produces(MediaType.TEXT_PLAIN)
	public String getFileClassName(@QueryParam("idTask") String idTask){
		UUID idTsk=UUID.fromString(idTask);
		String fclsName="";
		fclsName = super.sCtl.getFileClassName(idTsk);
		return fclsName;
	}
	
	@GET
	@Path("getJarName")
	@Produces(MediaType.TEXT_PLAIN)
	public String getJarName(@QueryParam("idTask") String idTask){
		UUID idTsk=UUID.fromString(idTask);
		String JarName="";
		JarName = super.sCtl.getJarName(idTsk);
		return JarName;
	}
	
	@GET
	@Path("getClassToProcess")
	//@Produces("multipart/mixed")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public byte[] getClassToProcess(@QueryParam("idTask") String idTask){
		UUID idTsk = UUID.fromString(idTask);
		byte[] clsToProcess=super.sCtl.getClassToProcess(idTsk);
		return clsToProcess;
	}
	
	@GET
	@Path("getObjToProcess")
	//@Produces("multipart/mixed")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public byte[] getObjToProcess(@QueryParam("idTask") String idTask){
		UUID idTsk = UUID.fromString(idTask);
		byte[] objToProcess=null;
		try {
			objToProcess = super.sCtl.getObjToProcess(idTsk);
		} catch (serverException e) {
			e.printStackTrace();
		}

		return objToProcess;
	}
	
	
	@PUT
	@Path("uploadProcessedObj")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public Response uploadProcessedObj(@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("idTask") String idTask){
		System.out.println("IDTASK: " + idTask);
		UUID idTsk = UUID.fromString(idTask);
		byte [] processedObj=null;
		try {
			processedObj = IOUtils.toByteArray(fileInputStream);
		} catch (IOException e1) {
			//return e1.getMessage();
		}
		
		try {
			super.sCtl.setProcessedObj(idTsk, processedObj);
		} catch (serverException e) {
			e.printStackTrace();
		}
		
		return Response.ok().build();
		
	}
	
	

	
}
