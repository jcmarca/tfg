package WebServices;



import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.MultiPart;
import Server.serverException;

@Path("/provider/")
public class provider extends absService {
	
	@PUT
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("fileClass")
	public String uploadFileClass(@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("idProject") String idProject,
			@FormDataParam("className") String className){
		UUID idFileClass=null;
		UUID idPrj = UUID.fromString(idProject);
		byte[] clsFile;
		try {
			clsFile = IOUtils.toByteArray(fileInputStream);
		} catch (IOException e1) {
			//returns error throw http respond
			//e1.printStackTrace();
			return e1.getMessage();
		}
		
		try {
			idFileClass = super.sCtl.newFileClass(idPrj, clsFile, className);
		} catch (serverException e) {
			//returns error throw http respond
			//e.printStackTrace();
			return e.getMessage();
		}
		return idFileClass.toString();
	}
	
	@PUT
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("uploadObj")
	public String uploadObjToProcess(@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("idProject") String idProject,
			@FormDataParam("idFileClass") String idFileClass){
		
		UUID idPrj = UUID.fromString(idProject);
		UUID idFClass = UUID.fromString(idFileClass);
		UUID idTask=null;
		byte[] objToProcess;
		try {
			objToProcess = IOUtils.toByteArray(fileInputStream);
		} catch (IOException e1) {
			//returns error throw http respond
			//e1.printStackTrace();
			return e1.getMessage();
		}
		try {
			idTask=super.sCtl.newTask(idPrj, objToProcess, idFClass);
		} catch (serverException e) {
			return e.getMessage();
		}
		return idTask.toString();
	}

	@GET
	@Produces("multipart/mixed")
	@Path("getProcessedObj")
	public Response getProcessedObj(@QueryParam("idTask") String idTask){
		UUID idTsk = UUID.fromString(idTask);
		byte[] processedObj=super.sCtl.getProcessedObj(idTsk);
		if (processedObj!=null){
			InputStream fileInputStream = new ByteArrayInputStream(processedObj);
			MultiPart objMultiPart = new MultiPart();
			objMultiPart.type(new MediaType("multipart", "mixed"));
			objMultiPart.bodyPart(fileInputStream, new MediaType("multipart", "mixed"));
			return Response.ok(objMultiPart).build();
		}
		return Response.noContent().build();
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("delTask")
	public String deleteTask(@QueryParam("idTask") String idTask){
		UUID idTsk = UUID.fromString(idTask);
		try {
			super.sCtl.deleteTask(idTsk);
		} catch (serverException e) {
			e.printStackTrace();
		}
		return "OK";
	}
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("project")
	public String getProjectId(){
		UUID idProject=sCtl.createProject();
		return idProject.toString();
	}
}


