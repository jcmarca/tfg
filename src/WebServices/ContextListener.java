package WebServices;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import Server.serverController;
import Server.serverException;

public class ContextListener implements ServletContextListener {

	private serverController sc;
	
	@Override
	public void contextInitialized(ServletContextEvent sce){
		
		//loads the domain controller
		sc= serverController.getServerController();
		
		sc.loadConfig();
		
		try {
			sc.loadDataFromDB();
		} catch (serverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
		
	}
}
